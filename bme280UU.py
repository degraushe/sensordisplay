def read_bme280(NN=None, DEBUG=False):
    """Reading values from BME280 sensor.

    required: /boot/config.txt - dtoverlay=i2c-sensor,bme280
    parameter: local altitude - allowed numeric input values are -500..9000
    returns an array with temperature, pressure and rel. humidity
    """         
    # check parameters
    if NN is not None:
        if type(NN) is not int or NN < -500 or NN > 9000:
            raise TypeError("NN value must be -500..9000")
    #
    from pathlib import Path
    # check iio attached devices
    p = Path('/sys/bus/iio/devices')
    for device in list(p.glob('iio:device?')):
        # sensor name = bme280 ?
        with open( str(device) + '/name', 'r') as name:
            sensor_name = name.read().strip()
            if sensor_name != 'bme280':
                if DEBUG: print( 'Sensor skipped   : ', sensor_name )
                continue
            if DEBUG: print( 'Sensor found     : ', sensor_name )
        # read temperature
        with open( str(device) + '/in_temp_input', 'r') as value:
            temperature = float(value.read()) / 1000
            if DEBUG: print( 'Temperature in °C: ', temperature )
        # read pressure
        with open( str(device) + '/in_pressure_input', 'r') as value:
            pressure = float(value.read()) * 10
            if DEBUG: print( 'Pressure in hPa  : ', pressure )
        # pressure at sea level
        if NN is not None:
            pressure = pressure / pow( ( 273.15 + temperature ) /
                       ( 273.15 + temperature + 0.0065 * NN ), 5.255 )
            if DEBUG: print( 'Pressure NN hPa  : ', pressure )
        # read humidity
        with open( str(device) + '/in_humidityrelative_input', 'r') as value:
            humidity = float(value.read()) / 1000
            if DEBUG: print( 'Humidity in %rel.: ', humidity )
        # bme280 done
        return round(temperature,2), round(pressure, 2), round(humidity, 2)
    # not found -> return None
    if DEBUG: print ('sensor BME280 not found !')
    return


