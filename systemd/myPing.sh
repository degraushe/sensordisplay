#!/usr/bin/bash
INSTALLDIR=`pwd`

cat <<EOF | sudo tee /etc/systemd/system/myPing.service
[Unit]
Description=my Ping as a service
After=mosquitto.service

StartLimitIntervalSec=180
StartLimitBurst=3

[Service]
Type=simple
User=pi
Group=pi
WorkingDirectory=${INSTALLDIR}
ExecStart=/usr/bin/python3 -u PingMQTT.py
ExecReload=/bin/kill -HUP \$MAINPID
Restart=on-failure
RestartSec=30s
StandardOutput=file:${INSTALLDIR}/log/PingMQTT.log 
StandardError=file:${INSTALLDIR}/log/PingMQTT.err

[Install]
WantedBy=multi-user.target

EOF

sudo systemctl daemon-reload
sudo systemctl enable myPing
sudo systemctl start myPing
sudo systemctl status myPing
# end

