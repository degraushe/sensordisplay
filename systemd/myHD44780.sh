#!/usr/bin/bash
INSTALLDIR=`pwd`

cat <<EOF | sudo tee /etc/systemd/system/myHD44780.service
[Unit]
Description=my HD44780 display as a service
After=mosquitto.service

StartLimitIntervalSec=180
StartLimitBurst=3

[Service]
Type=simple
User=pi
Group=pi
WorkingDirectory=${INSTALLDIR}
ExecStart=/usr/bin/python3 -u HD44780MQTT.py
ExecReload=/bin/kill -HUP \$MAINPID
Restart=on-failure
RestartSec=30s
StandardOutput=file:${INSTALLDIR}/log/HD44780MQTT.log  
StandardError=file:${INSTALLDIR}/log/HD44780MQTT.err

[Install]
WantedBy=multi-user.target

EOF

sudo systemctl daemon-reload
sudo systemctl enable myHD44780
sudo systemctl start myHD44780
sudo systemctl status myHD44780


