#!/usr/bin/bash
INSTALLDIR=`pwd`

cat <<EOF | sudo tee /etc/systemd/system/myCsvFileLog.service
[Unit]
Description=my CsvFileLog as a service
After=mosquitto.service

StartLimitIntervalSec=180
StartLimitBurst=3

[Service]
Type=simple
User=pi
Group=pi
WorkingDirectory=${INSTALLDIR}
ExecStart=/usr/bin/python3 -u CsvFileLogMQTT.py
ExecReload=/bin/kill -HUP \$MAINPID
Restart=on-failure
RestartSec=30s
StandardOutput=file:${INSTALLDIR}/log/CsvFileLogMQTT.log 
StandardError=file:${INSTALLDIR}/log/CsvFileLogMQTT.err

[Install]
WantedBy=multi-user.target

EOF

sudo systemctl daemon-reload
sudo systemctl enable myCsvFileLog
sudo systemctl start myCsvFileLog
sudo systemctl status myCsvFileLog
# end

