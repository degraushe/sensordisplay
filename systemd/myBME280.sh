#!/usr/bin/bash
INSTALLDIR=`pwd`

cat <<EOF | sudo tee /etc/systemd/system/myBME280.service
[Unit]
Description=my BME280 sensor as a service
After=mosquitto.service

StartLimitIntervalSec=180
StartLimitBurst=3

[Service]
Type=simple
User=pi
Group=pi
WorkingDirectory=${INSTALLDIR}
ExecStart=/usr/bin/python3 -u BME280MQTT.py
ExecReload=/bin/kill -HUP \$MAINPID
Restart=on-failure
RestartSec=3s
StandardOutput=file:${INSTALLDIR}/log/BME280MQTT.log   
StandardError=file:${INSTALLDIR}/log/BME280MQTT.err

[Install]
WantedBy=multi-user.target

EOF

sudo systemctl daemon-reload
sudo systemctl enable myBME280
sudo systemctl start myBME280
sudo systemctl status myBME280

