#!/usr/bin/bash
INSTALLDIR=`pwd`

cat <<EOF | sudo tee /etc/systemd/system/myBH1750.service
[Unit]
Description=my BH1750 sensor as a service
After=mosquitto.service

StartLimitIntervalSec=180
StartLimitBurst=3

[Service]
Type=simple
User=pi
Group=pi
WorkingDirectory=${INSTALLDIR}
ExecStart=/usr/bin/python3 -u BH1750MQTT.py
ExecReload=/bin/kill -HUP \$MAINPID
Restart=on-failure
RestartSec=30s
StandardOutput=file:${INSTALLDIR}/log/BH1750MQTT.log    
StandardError=file:${INSTALLDIR}/log/BH1750MQTT.err

[Install]
WantedBy=multi-user.target

EOF

sudo systemctl daemon-reload
sudo systemctl enable myBH1750
sudo systemctl start myBH1750
sudo systemctl status myBH1750
