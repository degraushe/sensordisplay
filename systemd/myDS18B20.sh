#!/usr/bin/bash
INSTALLDIR=`pwd`

cat <<EOF | sudo tee /etc/systemd/system/myDS18B20.service
[Unit]
Description=my DS18B20 sensor as a service
After=mosquitto.service

StartLimitIntervalSec=180
StartLimitBurst=3

[Service]
Type=simple
User=pi
Group=pi
WorkingDirectory=${INSTALLDIR}
ExecStart=/usr/bin/python3 -u DS18B20MQTT.py
ExecReload=/bin/kill -HUP \$MAINPID
Restart=on-failure
RestartSec=30s
StandardOutput=file:${INSTALLDIR}/log/DS18B20MQTT.log 
StandardError=file:${INSTALLDIR}/log/DS18B20MQTT.err

[Install]
WantedBy=multi-user.target

EOF

sudo systemctl daemon-reload
sudo systemctl enable myDS18B20
sudo systemctl start myDS18B20
sudo systemctl status myDS18B20

