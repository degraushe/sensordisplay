def read_cpu_temperature():
    """Reading CPU temperature from /sys.
    returns temperature in °C
    """         
    DEBUG = True
    #
    # read value 
    with open( '/sys/class/thermal/thermal_zone0/temp', 'r') as value:
        temperature = float(value.read()) / 1000
        if DEBUG: print( 'CPU temperature  : ', temperature )
  # CPU temperature done
    return round( temperature, 2 )
