"""SensorDisplay: module for JSON configuration store.

helper functions to retrieve configuration from JSON
file store by other modules
checking JSON file structue if stated by its own

source: https://gitlab.com/degraushe/sensordisplay
file: <installdir>/sensordisplay/configuration.py
"""

class read_configuration():
    """ reading JSON file content into dictionary.

    fixed file store location for now is
    /home/pi/sensordisplay.json
    
    extracting general configuration items for all modules
    DEBUG: True/False
    start_seconds: point(s) in time to do measurement
    value2log: activate messages to store values in CSV log
    value2aio: avtivate messages to be forwarded to Adafruit IO
    used as super class for sensor module specific items
    """
    def __init__(self):
        import json
        configuration_file = '/home/pi/sensordisplay.json'
        self.configuration={}
        try:
            with open( configuration_file, 'r') as f:
                self.configuration = json.load(f)
        except json.decoder.JSONDecodeError as err:
            print("JSON syntax error: {0}".format(err))
            exit(2)
        except:
            print( "error reading JSON file: " + configuration_file)
            #print( "using defaults !" )
            exit(2)
    
    def get_general_values(self):
        """ reading general configuration items from dictionary.

        extracting general configuration items for all modules
        DEBUG: True/False
        start_seconds: point(s) in time to do measurement
        value2log: activate messages to store values in CSV log
        value2aio: avtivate messages to be forwarded to Adafruit IO
        """
        self.DEBUG=( self.configuration["debug"]=="true" ) if "debug" in self.configuration else False
        self.start_seconds = [ int(value) for value in self.configuration["start_seconds"].split(',')
                              ] if "start_seconds" in self.configuration else [ 0 ]
        self.value2log=( self.configuration["value2log"]=="true" ) if "value2log" in self.configuration else False
        self.value2aio=( self.configuration["value2aio"]=="true" ) if "value2aio" in self.configuration else False
        
class read_HD44780_configuration(read_configuration):
    """ extracting HD4470 specific configuration items.

    extending function 'read_configuration'
    DEBUG: True/False
    gradC: utf-8 coded value to display '°C' on HD4470
    """

    def __init__(self):
        read_configuration.__init__(self) 
        #
        self.configuration=self.configuration["HD44780"] if "HD44780" in self.configuration else {}
        #
        self.DEBUG=( self.configuration["debug"]=="true" ) if "debug" in self.configuration else False
        self.gradC=self.configuration["gradC"] if "gradC" in self.configuration else "\xDFC"

class read_CsvFileLog_configuration(read_configuration):
    """ extracting CsvFileLog specific configuration items.

    extending function 'read_configuration'
    DEBUG: True/False
    filePath: directory path for CSV files
    generations: number of daily generations to keep, maximum 9
    """

    def __init__(self):
        read_configuration.__init__(self) 
        #
        self.configuration=self.configuration["CsvFileLog"] if "CsvFileLog" in self.configuration else {}
        #
        self.DEBUG=( self.configuration["debug"]=="true" ) if "debug" in self.configuration else False
        self.filePath=self.configuration["filePath"] if "filePath" in self.configuration else "/home/pi/sensordisplay/csv"
        self.generations=self.configuration["generations"] if "generations" in self.configuration else 7
        #
        from pathlib import Path

        if not Path(self.filePath).is_dir():
            print( "error: configured CSV log file path doesn't exist !" )
            sys.exit(2)
        if Path(self.filePath).is_file():
            print( "error: configured CSV log file path is a file !" )
            sys.exit(2)


class read_BME280_configuration(read_configuration):
    """ extracting BME280 specific configuration items.

    extending function 'read_configuration'
    altitude_NN: heigth of location to calculate pressure on sea level
    """

    def __init__(self):
        read_configuration.__init__(self) 
        #
        self.configuration=self.configuration["BME280"] if "BME280" in self.configuration else {}
        #
        self.get_general_values()
        self.altitude_NN=self.configuration["altitude_NN"] if "altitude_NN" in self.configuration else 0
    
class read_DS18B20_configuration(read_configuration):
    """ extracting DS18B20 specific configuration items.

    extending function 'read_configuration'
    scroll_wait: delay of secods betwen display pages
    sensors: list of DS18B20 sensor ID's, names and calibration values
    conv_time_max: calculated maximum value for 'conv_wait' time of congigured DS18B20 sensors
    """

    def __init__(self):
        read_configuration.__init__(self) 
        #
        self.configuration=self.configuration["DS18B20"] if "DS18B20" in self.configuration else {}
        #
        self.get_general_values()
        self.scroll_wait=self.configuration["scroll_wait"] if "scroll_wait" in self.configuration else 5
        self.sensors=()
        if "Sensors" in self.configuration:
            for name, data in self.configuration["Sensors"].items():
                id=data["id"]
                adjust=data["adjust"] if "adjust" in data else 0
                self.sensors +=((id,name,adjust),)
        # determine wait time for DS18B20 meassurement
        self.conv_time_max = 0
        for id, name, adjust in self.sensors:
            try:
                with open( '/sys/bus/w1/devices/' + id + '/conv_time', 'r') as value:
                    self.conv_time_max = max(self.conv_time_max, int(value.read()) )
            except:
                self.conv_time_max = 1000

class read_BH1750_configuration(read_configuration):
    """ extracting BH1750 specific configuration items.

    extending function 'read_configuration'
    """

    def __init__(self):
        read_configuration.__init__(self) 
        #
        self.configuration=self.configuration["BH1750"] if "BH1750" in self.configuration else {}
        #
        self.get_general_values()

class read_Ping_configuration(read_configuration):
    """ extracting Ping specific configuration items.

    extending function 'read_configuration'
    hosts: list of name or address entries for target host interfaces
    timeout: 'ping' - timeout
    """

    def __init__(self):
        read_configuration.__init__(self) 
        #
        self.configuration=self.configuration["Ping"] if "Ping" in self.configuration else {}
        #
        self.get_general_values()
        self.scroll_wait=self.configuration["scroll_wait"] if "scroll_wait" in self.configuration else 5
        self.hosts=()
        if "hosts" in self.configuration:
            for name, address in self.configuration["hosts"].items():
                self.hosts +=((name,address),)
        self.timeout=self.configuration["timeout"] if "timeout" in self.configuration else 1.0

# testing of json structure
if __name__ == "__main__":
    import sys
    if len( sys.argv ) == 1:
        print("executing configuration file test reading ...")
        test=read_configuration()
    else:
        if sys.argv[1].upper() == "HD44780":
           print("executing HD44780 configuration test reading ...")
           test=read_HD44780_configuration()
        elif sys.argv[1].upper() == "CSVFILELOG":
           print("executing CsvFileLog configuration test reading ...")
           test=read_CsvFileLog_configuration()
        elif sys.argv[1].upper() == "BME280":
           print("executing BME280 configuration test reading ...")
           test=read_BME280_configuration()
        elif sys.argv[1].upper() == "BH1750":
           print("executing BH1750 configuration test reading ...")
           test=read_BH1750_configuration()
        elif sys.argv[1].upper() == "DS18B20":
           print("executing DS18B20 configuration test reading ...")
           test=read_DS18B20_configuration()
        elif sys.argv[1].upper() == "PING":
           print("executing Ping configuration test reading ...")
           test=read_Ping_configuration()
        else:
           print("error: specified component not found: ",sys.argv[1])
           exit(2)
    #
    print(test.__dict__)
    print("no syntax / configuration issues found")
    exit(0)

        
