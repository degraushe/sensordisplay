def read_bh1750(DEBUG=False):
    """Reading values from BH1750 sensor.

    required: /boot/config.txt - dtoverlay=i2c-sensor,bh1750,addr=<address>
            address 0x23 ( default ) or 0x5C (pull to VCC)
    returns illuminance in lux
    """         

    from pathlib import Path
    # check iio attached devices
    p = Path('/sys/bus/iio/devices')
    for device in list(p.glob('iio:device?')):
        # sensor name = bh1750 ?
        with open( str(device) + '/name', 'r') as name:
            sensor_name = name.read().strip()
            if sensor_name != 'bh1750':
                if DEBUG: print( 'Sensor skipped   : ', sensor_name )
                continue
            if DEBUG: print( 'Sensor found     : ', sensor_name )
        # read raw value 
        with open( str(device) + '/in_illuminance_raw', 'r') as value:
            illuminance_raw = float(value.read())
            if DEBUG: print( 'Illuminance raw  : ', illuminance_raw )
        # read scale
        with open( str(device) + '/in_illuminance_scale', 'r') as value:
            illuminance_scale = float(value.read())
            if DEBUG: print( 'Scale            : ', illuminance_scale )
        # calculate illuminance
        illuminance = int ( illuminance_raw * illuminance_scale )
        if DEBUG: print( 'Illuminance - Lux: ', illuminance )
      # bh1750 done
        return illuminance
    # not found -> return None
    if DEBUG: print ('sensor BH1750 not found !')
    return


