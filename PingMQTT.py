"""SensorDisplay: module for network connectivity check.

requires icmplib module, install with 'sudo pip3 install icmplib'

get availabilty of a list of hosts
- nane: hostname or ip address
and publish through local MQTT broker instance

source: https://gitlab.com/degraushe/sensordisplay
file: <installdir>/sensordisplay/PingMQTT.py
"""

import paho.mqtt.client as mqtt
import time
import datetime
import sys
import signal
from configuration import read_Ping_configuration
from icmplib import ping
from math import nan, isnan

## initializing
lcd_topic = "SensorDisplay/HD44780"
log_topic = "SensorDisplay/CsvFileLog/ping"
aio_topic = "SensorDisplay/AdafruitIO/ping"

## reading configuration
cf=read_Ping_configuration()

# reload of configuration on SIGHUP
def reload_configuration(sig, frame):
    global cf
    cf=read_Ping_configuration()
    if cf.DEBUG: print("reloaded configuration")
signal.signal(signal.SIGHUP, reload_configuration)

# clean exit on process termination
def signal_handler(sig, frame):
    raise KeyboardInterrupt
signal.signal(signal.SIGTERM, signal_handler)

# MQTT client
client = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2,"Ping")

# The callback for when the client receives a CONNACK response from the server.
@client.connect_callback()
def on_connect(client, userdata, flags, reason_code, properties):
    if cf.DEBUG: print(f"Connected with result code {reason_code}")

# The callback for clean up on disconnect called.
@client.disconnect_callback()
def on_disconnect(client, userdata, flags, reason_code, properties):
    if reason_code == 0:
        if cf.DEBUG: print("Bye")
    else:
        if cf.DEBUG: print("Unexpected disconnection.")
    
# connect to MQTT
client.connect("localhost", 1883, 60)
client.loop_start()

# messure loop
while True:
    try:
        t = datetime.datetime.now()
        if t.second % 60 in cf.start_seconds:
            if cf.DEBUG:
                print("start second = {} of {}".format(t,cf.start_seconds))
            # process all hosts 
            messages = []
            for name, address in cf.hosts:
                # host ping check
                host_rtt = nan
                try:
                    host = ping(address, count=1, timeout=cf.timeout,
                                privileged=False )
                except:
                    pass
                else:
                    if host.is_alive: host_rtt = host.avg_rtt
                messages.append( '{:<10s}:{:6.1f} ms'
                                 .format(name[0:10], host_rtt))
                if cf.DEBUG: print("ping to {} ({}): {:.1f} ms"
                                   .format(name, address, host_rtt))
                # send data recording values in csv format
                if cf.value2log:
                    if cf.DEBUG: print("publishing to log")
                    client.publish(log_topic,
                                   '{:%d.%m.%Y %H:%M:%S};{:s};{:s};{:.1f}'
                                   .format(t, name, address, host_rtt))
                # send data to Adaruit IO via MQTT bridge
                if cf.value2aio:
                    if cf.DEBUG: print("publishing to AIO")
                    if not isnan(host_rtt):
                        client.publish('{}/{}'.format(aio_topic, name),
                                       '{:.1f}'.format(host_rtt) )   
            # send to display
            if cf.DEBUG: print( 'HD44780 page publishing ...' )
            i = 0
            for message in messages:
                # scroll wait after 3 messages processed
                if i == 4:
                    time.sleep(cf.scroll_wait)
                    if cf.DEBUG: print( 'scroll wait for {} seconds'
                                        .format(cf.scroll_wait))
                # increment line index 
                i = i % 4 + 1
                if i == 1:
                    # 1st line contains time stamp
                    client.publish('{}/Zeile1'.format(lcd_topic),
                                   '{:%d.%m.%Y  %H:%M:%S}'.format(t))
                    i += 1
                # process line message
                client.publish( lcd_topic + "/Zeile" + str(i), message )
                client.publish('{}/Zeile{}'.format(lcd_topic, i), message )
            # fill unused lines with blanks
            for j in range( i+1,5 ):
                client.publish('{}/Zeile{}'.format(lcd_topic, j), " " )
            #
            if cf.DEBUG: print( 'HD44780 pages published' )        #
        #
        time.sleep(1)
    except KeyboardInterrupt:
        break
    
# finalyse
client.publish( '{}/status'.format(lcd_topic),"clear" )   
client.loop_stop()
time.sleep(2)
client.disconnect()
#sys.exit(0)

