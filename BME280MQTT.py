"""SensorDisplay: module for BME280 sensor.

get temperature, pressure and humidity measurements
from BME280 sensor and
publish through local MQTT broker instance

using helper function in module bme280UU.py

requires IIO kernel activation in /boot/config.txt:
dtoverlay=i2c-sensor,bme280

source: https://gitlab.com/degraushe/sensordisplay
file: <installdir>/sensordisplay/BME280MQTT.py
"""

import paho.mqtt.client as mqtt
import time
import datetime
import sys
import signal
from bme280UU import read_bme280
from configuration import read_BME280_configuration

## initializing
lcd_topic = "SensorDisplay/HD44780"
log_topic = "SensorDisplay/CsvFileLog/BME280"
aio_topic = "SensorDisplay/AdafruitIO/BME280"

## reading configuration
cf=read_BME280_configuration()

# reload of configuration on SIGHUP
def reload_configuration(sig, frame):
    global cf
    cf=read_BME280_configuration()
    if cf.DEBUG: print("reloaded configuration")
signal.signal(signal.SIGHUP, reload_configuration)

# clean exit on process termination
def signal_handler(sig, frame):
    raise KeyboardInterrupt
signal.signal(signal.SIGTERM, signal_handler)

# MQTT client
client = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2,"BME280")

# The callback for when the client receives a CONNACK response from the server.
@client.connect_callback()
def on_connect(client, userdata, flags, reason_code, properties):
    if cf.DEBUG: print(f"Connected with result code {reason_code}")

# The callback for clean up on disconnect called.
@client.disconnect_callback()
def on_disconnect(client, userdata, flags, reason_code, properties):
    if reason_code == 0:
        if cf.DEBUG: print("Bye")
    else:
        if cf.DEBUG: print("Unexpected disconnection.")
    
# connect to MQTT
client.connect("localhost", 1883, 60)
client.loop_start()

# messure loop
while True:
    try:
        t = datetime.datetime.now()
        if t.second % 60 in cf.start_seconds:
            # read values from sensor
            temperature,pressure,humidity = read_bme280(cf.altitude_NN,
                                                        cf.DEBUG)
            # send display information
            client.publish('{}/Zeile1'.format(lcd_topic),
                           '{:%d.%m.%Y  %H:%M:%S}'.format(t))
            client.publish('{}/Zeile2'.format(lcd_topic),
                           'Temp.  : {:.1f} °C'.format(temperature))  
            client.publish('{}/Zeile3'.format(lcd_topic),
                           'Druck  : {:.0f} hPa NN'.format(pressure))   
            client.publish('{}/Zeile4'.format(lcd_topic),
                           'Feuchte: {:.1f} % rel.'.format(humidity))
            # send data recording values in csv format
            if cf.value2log:
                if cf.DEBUG: print("publishing to log")
                client.publish(log_topic,
                               '{:%d.%m.%Y %H:%M:%S};{:.1f};{:.0f};{:.1f}'.format(
                                t, temperature, pressure, humidity))
            # send data to Adaruit IO via MQTT bridge
            if cf.value2aio:
                if cf.DEBUG: print("publishing to AIO")
                client.publish('{}/temperature'.format(aio_topic),
                               '{:.1f}'.format(temperature) )   
                client.publish('{}/pressure'.format(aio_topic),
                               '{:.0f}'.format(pressure) )   
                client.publish('{}/humidity'.format(aio_topic),
                               '{:.1f}'.format(humidity) )   
      #
        time.sleep(1)
    except KeyboardInterrupt:
        break
    
# finalyse
client.publish( '{}/status'.format(lcd_topic),"clear" )   
client.loop_stop()
time.sleep(2)
client.disconnect()
sys.exit(0)

