"""SensorDisplay: module for HD44780 2004 display output.

get display lines from local MQTT broker instance and
writes to HD44780 2004 display connected via I2C.

using 3rd party HD44780 LCD library in module lcdlib.py

source: https://gitlab.com/degraushe/sensordisplay
file: <installdir>/sensordisplay/HD44780MQTT.py
"""

import paho.mqtt.client as mqtt
import time
import datetime
import sys
import signal
from configuration import read_HD44780_configuration

## initializing
lcd_topic = "SensorDisplay/HD44780"

## reading configuration
cf=read_HD44780_configuration()

if cf.DEBUG: print( "HD44780 code for °C: ", cf.gradC )

# reload of configuration on SIGHUP
def reload_configuration(sig, frame):
    global cf
    cf=read_HD44780_configuration()
    if cf.DEBUG: print("reloaded configuration")
signal.signal(signal.SIGHUP, reload_configuration)

# clean exit on process termination
def signal_handler(sig, frame):
    raise KeyboardInterrupt
signal.signal(signal.SIGTERM, signal_handler)

# Display - 2 different I2C adapter versions possible
import lcdlib
try:
    myLCD = lcdlib.lcd(0x27,4,20)
except:
    myLCD = lcdlib.lcd(0x3f,4,20)
myLCD.clear()

# misc
delay = 5.0

# MQTT client
client = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2,"HD44780")

# The callback for when the client receives a CONNACK response from the server.
@client.connect_callback()
def on_connect(client, userdata, flags, reason_code, properties):
    myLCD.clear()
    myLCD.display_string("connected to MQTT",0,0)
    if cf.DEBUG: print("Connected with result code "+str(rc))
    #
    client.subscribe( lcd_topic + "/#")

# The callback for when a PUBLISH message is received from the server.
@client.message_callback()
def on_message(client, userdata, msg):
    if cf.DEBUG: print(msg.topic+" "+str(msg.payload))
    topic = msg.topic.split('/')[-1].lower()
    if topic[0:5] == 'zeile':
        row = int(topic[5:6])
        if row in range(1,5):
            status = 'text' 
            text = str(msg.payload,"utf-8").replace( "°C", cf.gradC )
            myLCD.display_string(text[0:20].ljust(20),row-1,0)
    elif topic == 'status':
        status = str(msg.payload,"utf-8").lower().lstrip().rstrip()
        if status == 'clear':
            myLCD.clear()
            
# The callback for clean up on disconnect called.
@client.disconnect_callback()
def on_disconnect(client, userdata, flags, reason_code, properties):
    if reason_code == 0:
        if cf.DEBUG: print("Bye")
    else:
        if cf.DEBUG: print("Unexpected disconnection.")
    
# connect to MQTT
client.connect("localhost", 1883, 60)

# start MQTT threaded message loop
client.loop_start()

# display loop
while True:
    try:
        time.sleep(delay)
    except KeyboardInterrupt:
        break
    
# finalyse
myLCD.clear()
myLCD.display_string("MQTT disconnected",0,0)
client.loop_stop()
time.sleep(1)
client.disconnect()
sys.exit(0)
