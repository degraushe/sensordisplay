"""SensorDisplay: module for DS18B20 temperature sensors.

get temperature measurements from DS18B20 sensors and
publish through local MQTT broker instance

requires 1-wire activation in /boot/config.txt, gpio pin 4 is default set up:
dtoverlay=w1-gpio,gpiopin=4

source: https://gitlab.com/degraushe/sensordisplay
file: <installdir>/sensordisplay/DS18B20MQTT.py
"""

import paho.mqtt.client as mqtt
import time
import datetime
import sys
import signal
from subprocess import call
from configuration import read_DS18B20_configuration

## initializing
lcd_topic = "SensorDisplay/HD44780"
log_topic = "SensorDisplay/CsvFileLog/DS18B20"
aio_topic = "SensorDisplay/AdafruitIO/DS18B20"

## reading configuration
cf=read_DS18B20_configuration()

# reload of configuration on SIGHUP
def reload_configuration(sig, frame):
    global cf
    cf=read_DS18B20_configuration()
    if cf.DEBUG: print("reloaded configuration")
signal.signal(signal.SIGHUP, reload_configuration)

# clean exit on process termination by SIGTERM
def sigterm_handler(sig, frame):
    raise KeyboardInterrupt
signal.signal(signal.SIGTERM, sigterm_handler)

# MQTT client
client = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2,"DS18B20")

# The callback for when the client receives a CONNACK response from the server.
@client.connect_callback()
def on_connect(client, userdata, flags, reason_code, properties):
    if cf.DEBUG: print(f"Connected with result code {reason_code}")

# The callback for clean up on disconnect called.
@client.disconnect_callback()
def on_disconnect(client, userdata, flags, reason_code, properties):
    if reason_code == 0:
        if cf.DEBUG: print("Bye")
    else:
        if cf.DEBUG: print("Unexpected disconnection.")
    
# connect to MQTT
client.connect("localhost", 1883, 60)
client.loop_start()

# messure loop
while True:
    try:
        t = datetime.datetime.now()
        if t.second % 60 in cf.start_seconds:
            if cf.DEBUG: print("start second = {} of {}".format(t,cf.start_seconds))
            # trigger measurements and wait for result generation ( os call due to root requirement )
            call( 'echo trigger | sudo tee /sys/bus/w1/devices/w1_bus_master1/therm_bulk_read > /dev/null', shell=True)
            if cf.DEBUG: print("triggered bulk read, waiting for {} ms ...".format(cf.conv_time_max))
            time.sleep(cf.conv_time_max / 1000)
            # process all sensors 
            messages = []
            for id, name, adjust in cf.sensors:
                try:                    
                    with open( '/sys/bus/w1/devices/' + id + '/temperature', 'r') as value:
                        temperature = float(value.read()) / 1000 + adjust
                    messages.append( '{:<10s}: {:.2f} °C'.format(
                                    name[0:10], temperature))
                    if cf.DEBUG: print( 'reading from sensor {}: {:.2f} °C'.format(
                                    name, temperature))
                    #
                    # send data recording values in csv format
                    if cf.value2log:
                        if cf.DEBUG: print("publishing to log")
                        client.publish(log_topic,
                            '{:%d.%m.%Y %H:%M:%S};{};{:.3f}'.format(
                                t, name, temperature))
                    # send data to Adaruit IO via MQTT bridge
                    if cf.value2aio:
                        if cf.DEBUG: print("publishing to AIO")
                        client.publish('{}/{}'.format(aio_topic, id),
                                       '{:.3f}'.format(temperature) )   
                except:
                    messages.append( '{:<10s}: Fehler'.format(name[0:10]))
                    if cf.DEBUG: print( 'error reading from sensor {}'.format(name))
            # send to display
            if cf.DEBUG: print( 'HD44780 page publishing ...' )
            i = 0
            for message in messages:
                # scroll wait after 3 messages processed
                if i == 4:
                    time.sleep(cf.scroll_wait)
                    if cf.DEBUG: print( 'scroll wait for {} seconds'.format(cf.scroll_wait))
                # increment line index 
                i = i % 4 + 1
                if i == 1:
                    # 1st line contains time stamp
                    client.publish('{}/Zeile1'.format(lcd_topic),
                                   '{:%d.%m.%Y  %H:%M:%S}'.format(t))
                    i += 1
                # process line message
                client.publish( lcd_topic + "/Zeile" + str(i), message )
                client.publish('{}/Zeile{}'.format(lcd_topic, i), message )
            # fill unused lines with blanks
            for j in range( i+1,5 ):
                client.publish('{}/Zeile{}'.format(lcd_topic, j), " " )
            #
            if cf.DEBUG: print( 'HD44780 pages published' )        #
        #
        time.sleep(1)
    except KeyboardInterrupt:
        break
    
# finalyse
client.publish( '{}/status'.format(lcd_topic),"clear" )   
client.loop_stop()
time.sleep(2)
client.disconnect()
sys.exit(0)

