"""SensorDisplay: module for BH1750 lminance sensor.

get luminance measurement from BH1750 sensor and
publish through local MQTT broker instance

using helper function in module bh1750UU.py

requires IIO kernel activation in /boot/config.txt:
dtoverlay=i2c-sensor,bh1750

source: https://gitlab.com/degraushe/sensordisplay
file: <installdir>/sensordisplay/BH1750MQTT.py
"""

import paho.mqtt.client as mqtt
import time
import datetime
import sys
import signal
from bh1750UU import read_bh1750
from configuration import read_BH1750_configuration

## initializing
lcd_topic = "SensorDisplay/HD44780"
log_topic = "SensorDisplay/CsvFileLog/BH1750"
aio_topic = "SensorDisplay/AdafruitIO/BH1750"

## reading configuration
cf=read_BH1750_configuration()

# reload of configuration on SIGHUP
def reload_configuration(sig, frame):
    global cf
    cf=read_BH1750_configuration()
    if cf.DEBUG: print("reloaded configuration")
signal.signal(signal.SIGHUP, reload_configuration)

# clean exit on process termination
def signal_handler(sig, frame):
    raise KeyboardInterrupt
signal.signal(signal.SIGTERM, signal_handler)

# MQTT client 
client = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2,"BH1750")

# The callback for when the client receives a CONNACK response from the server.
@client.connect_callback()
def on_connect(client, userdata, flags, reason_code, properties):
    if cf.DEBUG: print(f"Connected with result code {reason_code}")

# The callback for clean up on disconnect called.
@client.disconnect_callback()
def on_disconnect(client, userdata, flags, reason_code, properties):
    if reason_code == 0:
        if cf.DEBUG: print("Bye")
    else:
        if cf.DEBUG: print("Unexpected disconnection.")
    
# connect to MQTT and process
client.connect("localhost", 1883, 60)
client.loop_start()

# messure loop
while True:
    try:
        t = datetime.datetime.now()
        if t.second % 60 in cf.start_seconds:
            # read sensor data
            brightness = read_bh1750(cf.DEBUG)
            # write sensor data to display
            client.publish('{}/Zeile1'.format(lcd_topic),
                           '{:%d.%m.%Y  %H:%M:%S}'.format(t))
            client.publish('{}/Zeile2'.format(lcd_topic),
                           'Licht  : {:.0f} Lux'.format(brightness))  
            client.publish('{}/Zeile3'.format(lcd_topic),
                           " ")   
            client.publish('{}/Zeile4'.format(lcd_topic),
                           " ")
            # send data recording values in csv format
            if cf.value2log:
                if cf.DEBUG: print("publishing to log")
                client.publish(log_topic,
                    '{:%d.%m.%Y %H:%M:%S};{:.0f}'.format(t, brightness))
            # send data to Adaruit IO via MQTT bridge
            if cf.value2aio:
                if cf.DEBUG: print("publishing to AIO")
                client.publish('{}/brightness'.format(aio_topic),
                               '{:.0f}'.format(brightness) )   
        #
        time.sleep(1)
    except KeyboardInterrupt:
        break
    
# finalyse
client.publish( '{}/status'.format(lcd_topic),"clear" )   
client.loop_stop()
time.sleep(2)
client.disconnect()
sys.exit(0)

