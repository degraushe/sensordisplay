# Sensordisplay
Version 1.2 
- mit zentraler Konfigurationsdatei im JSON Format
- angepaßt an Paho MQTT python client version 2

Anzeige von Sensorwerten auf einem HD44780 Display
- entworfen für Rapberry PI ( PI Zero ) mit Raspberry Pi OS ( Bullseye )
- lokale MQTT Installation wird zur Weitergabe der Sensorinformation an das Display eingesetzt
- die Anzeigeeinheit und die Sensoren werden durch jeweils eigene Python-Programme eingebunden
- systemd-Dienste werden zur Kontrolle der einzelnen Programme benutzt 

## Betriebssystem

Raspberry Pi OS Bullseye ist erforderlich, um die benötigte MQTT Mosquitto Version bereitzustellen. Die *Lite* Version ist ausreichend, empfohlen wird die Standardgraphikversion ohne zusätzliche Programme um ggfs. Thonny lokal benutzen zu können.

Betriebssystem auf den neuesten Stang bringen:

    sudo apt-get update
    sudo apt-get full-upgrade


## Hardware anschließen und konfigurieren
### Schnittstellen einschalten

Benötigt werden
- I2C für das Anzeigemodul und verschiedene Sensoren
- 1-Wire für den DS18B20 Temperatursensor

sowie für monitorfreien Betrieb
- SSH für Thonny Remotezugriff und Terminalfenster
- VNC für lokalen Thonny

Zur Aktivierung / Kontrolle in einem Terminalfenster: 

    sudo raspi-config

- 3 Interface Options / Schnittstellen 
    - I5 I2C
    - I7 1-Wire
    - I2 SSH
    - I3 VNC

Ein beim Schliessen angemerkter erforderlicher Neustart erfolgt später, da noch weiter Einstellungen erforderlich sind.

Diese erfolgen in der Datei */boot/config.txt* :

1. 1-Wire wird standardmässig am GPIO Pin 4 aktiviert, soll ein anderer Pin genutzt werden ist die Zeile  
*dtoverlay=w1-gpio*  
zu ergänzen, z.B. für Pin 5:  
*dtoverlay=w1-gpio,gpiopin=5*  
Mehrere 1-Wire Anschlüsse an verschiedenen GPIO Pins sind möglich durch hinzufügen weiterer w1-gpio - Einträge mit entsprechenden Pin-Nummern.

2. Für den Sensor BME280 ist die Unterstützung im Betriebssystem einzuschalten,  
dazu am Ende der Datei folgende Zeile hinzufügen:  
*dtoverlay=i2c-sensor,bme280*

3. Für den Sensor BH1750 ist die Unterstützung im Betriebssystem einzuschalten,  
dazu am Ende der Datei folgende Zeile hinzufügen:  
*dtoverlay=i2c-sensor,bh1750*

Ein Neustart ist nun erforderlich bei Änderungen der Schnittstellenkonfiguration.

### Anzeige and Sensoren anschließen

> Die GPIO Pins des Raspberry PI sind für den 3,3 V Betrieb ausgelegt, 5V an einem Anschluß führen zur Zerstörung.  
>Das Anzeigemodul sowie die Sensormodule sind entworfen für den 5V Betrieb, funktionieren aber auch mit 3.3 V Betriebsspannung.  
>>Für den 5V Betrieb ist unbedingt ein ( I2C-fähiger ) Pegelwandler zwischen Rechner und Module einzubauen.

#### I2C Komponenten

I2C Module am Standard-I2C ( SDA=2, SCL=3 ) anschliessen.  
Die Belegung der Ports kann dann mit folgendem Kommando überprüft werden:

    i2cdetect -y 1

- 23 markiert mit UU = BH1750 mit aktivem Kerneltriber
- 27 oder 3F = HD44780
- 76 markiert mit UU = BME280 mit aktivem Kerneltreiber

#### 1-Wire Komponenten

Die DS18B20 Sensoren werden am 1-Wire angeschlossen, hinzu kommt ein *4,7K* Widerstand zwischen Datenleitung und Betriesspannung Vcc.  
5V Betrieb ist auch hier mit Pegelwandler möglich.  
Zur Überprüfung der erkannten Sensoren und Ermittlung der Sensor-ID's das folgende Kommando ausführen:

    ls -d /sys/bus/w1/devices/28*

## MQTT Infrastruktur
Das MQTT Protokoll wird hier eingesetzt, damit die einzelnen Einheiten jeweils von einem eigenen Python-Programm bedient werden können.  
Die Kommunikation erfolgt durch den Austausch von Nachrichten ( messages ), der über eine zentrale Schaltstelle ( broker ) organisiert wird. 
Jede Nachricht wird beim Senden einem Thema ( topic ) zugeordnet.  
Die verschiedenen Themen sind in einer Baumstruktur angeordnet, für diese Anwendung wird folgender Themenbaum verwendet:
- SensorDisplay
    - HD44780
        - Zeile1
        - Zeile2
        - Zeile3
        - Zeile4
    - CsvFileLog
        - BME280
        - BH1750
        - DS18B20
        - ping
    - AdafruitIO
        - BME280
            - temperature
            - pressure
            - humidity
        - BH1750
            - brightness
        - ping
            - name
        - DS18B20
            - 28-xxxxxxxxxxxx  (ID der DS18B20 Sensoren)

Alle Nachrichten für 
- HD44780 werden auf der Anzeige ausgegeben
- CsvFileLog werden in CSV formatierte Dateien geschrieben
- AdafruitIO werden vom Broker zu Adafruit IO weitergeleitet

### Installation des MQTT Brokers

Als zentrale Schaltstelle ( Broker ) wird *Mosquitto* eingesetzt.  
In der Version 2.0 oder höher ist standardmäßig ein lokaler Betrieb ohne Konfiguration und Benutzerverwaltung vorhanden, damit sind keine weiteren Sicherheitsvorkehrungen erforderlich.  
Die Mosqitto Client-Programme sind hilfreich für erste Tests, aber nicht unbedingt erforderlich.

Die Installation erfolgt in einem Terminalfenster:

    sudo apt-get install mosquitto mosquitto-clients
    sudo systemctl status mosquitto

Die Installationsroutine legt einen systemd-Dienst für Mosquitto zum automatischen Start = 'enabled' an und started den Broker, was die Statusausgabe bestätigen sollte.

Die lokale Installation braucht keine weitere Konfiguration.
Zur besseren Lesbarkeit der Protokolldatei kann das Zeitstempelformat in einer Konfigurationsdatei festgelegt werden.

    cat <<EOF | sudo tee /etc/mosquitto/conf.d/local.conf
    log_timestamp_format %Y-%m-%dT%H:%M:%S
    EOF
    sudo systemctl restart mosquitto

### Installation der MQTT Python Bibliothek
Die Phyton-Programme nutzen für die MQTT-Aufrufe die Eclipse PAHO Bibliothek.

Die Installation erfolgt in einem Terminalfenster:

    sudo pip3 install paho-mqtt==2.0.0

## Pythonprogramme und weitere Dateien installieren

Alle benötigten Programme, Verzeichnisse und Befehlsfolgen sind als GIT Repository verfügbar.  
Das Installationsverzeichnis kann frei gewählt werden, muß aber dem Beutzer *pi* gehören und von diesem beschreibbar sein.

Die folgenden Schritte erzeugen das Installationsverzeichnis */home/pi/sensordisplay* im Heimatverzeichnis des Benutzers *pi*.

Die Installation erfolgt in einem Terminalfenster nach Anmeldung als Benutzer *pi*:

    cd
    git clone -b <branch> https://gitlab.com/degraushe/sensordisplay.git
    cd sensordisplay
    ls -l 

mit \<branch\> = main ( getested ) oder PI20171122 ( in Bearbeitung )

Die nun installierte Version kann später jederzeit mit folgenden 'git' Befehl aktualisiert werden:

    cd
    cd sensordisplay
    git pull origin <branch>

mit \<branch\> = main ( getested ) oder PI20171122 ( in Bearbeitung )

** WICHTIG: Das aktuelle Verzeichnis ist für die weiteren Schritte beizubehalten. **

## Zentrale Konfigurationsdatei anlegen

Mit der aktuellen Version werden alle Konfigurationsparameter in einer zentralen Konfigurationsdatei im JSON Format abgelegt. 
Die Konfigurationsdaten werden von den einzelnen Programmen beim Start ausgelesen, Änderungen können von laufenden Programmen mit der *reload* Option des *systemctl* Befehls aktiviert werden.

    cp sensordisplay.json.template $HOME/sensordisplay.json

Das Template enthält Beispiele für alle zur Zeit möglichen Komponenten. Besonderheiten des JSON Formats wie *"true"* oder *"false"* für logische Werte, die Blockstruktur mit *{ ... : ... }* sowie Groß-/Kleinschreibung sind zu beachten. 

Die Struktur und der Inhalt der Konfigurationsdatei kann geprüft werden, generell oder einzeln mit dem Sensortyp als Parameter:

    python3 configuration.py { HD44780 | BH1750 | BME280 | DS18B20 | Ping | CsvFileLog }

Die konfigurierbaren Parameter sowie die Voreinstellungen ( default ) werden bei den einzelnen Komponentenprogrammen beschrieben.

 
## Komponentenprogramme als Hintergrunddienste

Jeder Sensor sowie Anzeigeeinheit und weitere Funktionen werden von einem eigenen Python-Programm gesteuert, das über den *systemd* automatisiert als Hintergrundprogramm gestartet wird.  

Mit *systemctl* Befehlen kann die Ausführung der einzelnen Dienste kontrolliert werden, die generelle Syntax ist 

- *sudo systemctl \<Option\> \<Dienst\>* mit den Optionen
    - start : started den Dienst
    - stop : beendet das Dienstprogramm
    - enable : konfiguriert den Dienst für den automatischen Start beim Rechnerstart
    - disable : entfernt die automatische Startoption
    - status : zeigt an, ob und wie das Dienstprogramm gestartet wurde

Die Dienstbezeichnungen nach dem Schema *my\<Dienst\>* werden bei den einzelnen Komponentenprogrammen genannt.

### Anzeigeeinheit HD44780

Das HD44780 ist eine 4 zeilige Anzeigeeinheit, mit 20 Zeichen pro Zeile.  
Bei der Anzeige für *°C* sind verschiedene Zeichencodes für *°* erforderlich, DF oder B2.   
Es existieren (mindestens) 2 Versionen des I2C Ansteuerungsmoduls, die auf verschiedene I2C Ports konfiguriert sind, 27 oder 3F.  

Die Anzeigeanheit wird vollständig vom Programm HD44780MQTT.py gesteuert.  
Für die *low level* Kommunikation wird dabei auf die Bibliothek *lcdlib.py* zurückgegriffen. 
Der benutzte I2C Port wird automatisch durch Austesten ermittelt.

Das Programm meldet sich beim MQTT-Broker an und wartet dann auf anzuzeigende Meldungen.

Die Konfiguration ist in der zentralen JSON Datei im Block *"HD44780"* einzustellen, mögliche Optionen sind
- "HD44780"
    - "debug" : "true" für ausführliche Meldungen im Log, default = "false" unterdrückt die meisten Meldungen
    - "gradC" : "\u00DFC" ( default ) oder "\u00B2C" je nach Displayvariante

Den Hintergrunddienst *myHD44780* in einem Terminalfenster anlegen und starten:

    bash systemd/myHD44780.sh
    
Bei erfolgreichem Start zeigt das Display *connected to MQTT* an.

### Helligkeitssensor BH1750

Der BH1750 misst die Umgebungshelligkeit in Lux. 

Der Sensor wird vollständig vom Programm BH1750MQTT.py gesteuert, 
der I2C Port 23 ist fest eingestellt. 

Zum konfigurierten Sekundenzeitpunkt wird der Meßwert ermittelt und Anzeigemeldungen an den MQTT-Broker geschickt, optional können Meldungen zur CSV Protokollierung oder zur Weiterleitung an Adafruit IO gesendet werden.  

Die Konfiguration ist in der zentralen JSON Datei im Block *"BH1750"* einzustellen, mögliche Optionen sind
- "BH1750"
    - "debug": "true" ( default ) für ausführliche Meldungen im Log, "false" unterdrückt die meisten Meldungen
    - "start_seconds" : eine oder mehrere Sekundenangaben, zu denen der Meßwert ermittelt wird, z.B. "0" ( default ) oder "10,30,50"
    - "value2log" : "true" aktiviert das Senden der Meßwerte zur Protokollierung in einer CSV-Datei, default = "false"
    - "value2aio" : "true" aktiviert das Senden der Meßwerte an Adafruit IO über eine MQTT Brückenverbindung, default = "false"

Den Hintergrunddienst *myBH1750* in einem Terminalfenster anlegen und starten:

    bash systemd/myBH1750.sh
    
### Wettersensor BME280

Der BME280 misst Temperatur, Luftdruck und Luftfeuchtigkeit. Der I2C Port 76 ist voreingestellt und wird als unveraendert angenommen.

Der Sensor wird vollständig vom Programm BM280MQTT.py gesteuert.  
Dabei erfolgt das Auslesen der Werte über die Bibliothek *bme280.py* unter Nutzung der vorher aktivierten Kernelschnittstelle. Das Leseprogramm unterstützt die Umrechnung des lokalen Luftdrucks auf den Luftdruck auf Meeresniveau bei Angabe der lokalen Höhe in m.  

Zum konfigurierten Sekundenzeitpunkt wird der Meßwert ermittelt und Anzeigemeldungen an den MQTT-Broker geschickt, optional können Meldungen zur CSV Protokollierung oder zur Weiterleitung an Adafruit IO gesendet werden.  

Die Konfiguration ist in der zentralen JSON Datei im Block *"BME280"* einzustellen, mögliche Optionen sind
- "BME280"
    - "debug" : "true" ( default ) für ausführliche Meldungen im Log, "false" unterdrückt die meisten Meldungen
    - "start_seconds" : eine oder mehrere Sekundenangaben, zu denen die Meßwerte ermittelt wird, z.B. "0" ( default ) oder "15,45"
    - "altitude_NN" : ganzzahliger Wert der Höhe der Meßstelle in *m über NN*, default = 0
    - "value2log" : "true" aktiviert das Senden der Meßwerte zur Protokollierung in einer CSV-Datei, default = "false"
    - "value2aio" : "true" aktiviert das Senden der Meßwerte an AdafruitIO über eine MQTT Brückenverbindung, default = "false"

Den Hintergrunddienst *myBME280* in einem Terminalfenster anlegen und starten:

    bash systemd/myBME280.sh
    
### Temperatursensoren DS18B20

Der DS18B20 misst die Temperatur mit einstellbarer Genauigkeit.  
Mehere Sensoren können an einen oder mehrere 1-Wire Anschlüsse angebunden werden.  
Jeder Sensor hat eine eigene ID mit 12-stelliger Nummer nach dem Muster *28-nnnnnnnnnnnn*.  
Zur Überprüfung der erkannten Sensoren und Ermittlung der Sensor-ID's ist das folgende Kommando im Terminalfenster ausführen:

    ls -d /sys/bus/w1/devices/28*

Die zu nutzenden Sensoren müssen **vorab** in der zentralen Konfigurationsdatei aufgelistet werden. Die Datei enthält Daten im JSON-Format, für jeden Sensor sind  

1. ein frei zu vergebender Name
2. die Sensor-ID
3. optional ein +/- Korrekturwert für die gemessene Temperatur

anzugeben nach dem Muster "Sensoranzeigename" : { "id":"28-xxxxxxxxxxxx", "adjust":0.5 }

Zum konfigurierten Sekundenzeitpunkt werden die Meßwerte der Sensoren ermittelt und Anzeigemeldungen an den MQTT-Broker geschickt, bei mehr als 3 Sensoren in mehreren Seiten mit einstellbarer Zwischenzeit.
Optional können Meldungen zur CSV Protokollierung oder zur Weiterleitung an Adafruit IO gesendet werden.  

Die Konfiguration ist in der zentralen JSON Datei im Block *"DS18B20"* einzustellen, mögliche Optionen sind

- "DS18B20"
    - "debug" : "true" ( default ) für ausführliche Meldungen im Log, "false" unterdrückt die meisten Meldungen
    - "start_seconds" : eine oder mehrere Sekundenangaben, zu denen die Meßwerte ermittelt wird, z.B. "30" ( default ) oder "15,45"
    - "value2log" : "true" aktiviert das Senden der Meßwerte zur Protokollierung in einer CSV-Datei, default = "false"
    - "value2aio" : "true" aktiviert das Senden der Meßwerte an AdafruitIO über eine MQTT Brückenverbindung, default = "false"
    - "Sensors" : JSON Block mit einem oder mehreren DS18B20 Konfigurationsblöcken nach obigem Schema, aufeinanderfolgende DS18B20 Blöcke durch Komma getrennt
    - "scroll_wait" : Ganzzahlwert in Sekunden als Wartezeit zwischen Anzeigeseiten bei mehr als 3 DS18B20 Sensoren, , default = 5

Beispiel für einen "Sensors" Block mit 2 Sensoren, Komma nach dem 1. Sensorblock zur Trennung, aber nicht am Ende der Kette:

    "Sensors" : 
        { 
            "Sensor 1" : { "id":"28-0319977995f2", "adjust":0.5},
            "Sensor 2" : { "id":"28-03209779c9c6", "adjust":-0.5}
        }

Den Hintergrunddienst *DS18B20* in einem Terminalfenster anlegen und starten:

    bash systemd/myDS18B20.sh
    
### Überwachung der Netzwerkverbindungen mit 'ping'

Zum Überwachen des Netzwerks nutzt dieses Modul *'ping'* zu mehreren Zielrechnern.
Die zu nutzenden Zielrechner müssen **vorab** in der zentralen Konfigurationsdatei aufgelistet werden. Die Datei enthält Daten im JSON-Format, für jeden Zielrechner sind  

1. ein frei zu vergebender Name ( ohne Leerstellen, bis zu 10 Stellen empfohlen )
2. der Rechnername oder die zu nutzende IP-Adresse

anzugeben nach dem Muster "name" : "hostname or ip-address"

Das Python Programm PingMQTT.py sammelt die Laufzeitdaten analog zu den Sensorprogrammen.
Die Laufzeit bis zum Eintreffen der Antwort auf 'ping' wird als Meßwert in ms ermittelt.
Im Fehlerfalle wird als Wert 'nan' = 'not a number' ausgegeben ( Python Standard ).

Das Modul benötigt eine spezielle Python-Bibliothek zum Ausführen vomn 'ping, diese ist zu installieren mit

    sudo pip3 install icmplib

Zum konfigurierten Sekundenzeitpunkt werden die Meßwerte ermittelt und Anzeigemeldungen an den MQTT-Broker geschickt, optional können Meldungen zur CSV Protokollierung oder zur Weiterleitung an Adafruit IO gesendet werden.  

Die Konfiguration ist in der zentralen JSON Datei im Block *"Ping"* einzustellen, mögliche Optionen sind
- "Ping"
    - "debug" : "true" ( default ) für ausführliche Meldungen im Log, "false" unterdrückt die meisten Meldungen
    - "start_seconds" : eine oder mehrere Sekundenangaben, zu denen die Meßwerte ermittelt wird, z.B. "45" ( default ) 
    - "value2log" : "true" aktiviert das Senden der Meßwerte zur Protokollierung in einer CSV-Datei, default = "false"
    - "value2aio" : "true" aktiviert das Senden der Meßwerte an AdafruitIO über eine MQTT Brückenverbindung, default = "false"
    - "scroll_wait" : Ganzzahlwert in Sekunden als Wartezeit zwischen Anzeigeseiten bei mehr als 3 Zielrechnern, , default = 5
    - "hosts" : JSON Block mit einem oder mehreren Zielhost Konfigurationsblöcken nach obigem Schema, aufeinanderfolgende Zielrechnerblöcke durch Komma getrennt
    - "timeout" : Timeout in Sekunden für 'ping', ist dieser erreicht wird 'nan' als Meßwert ausgegeben,  default = 0.5 s 

Beispiel für einen "hosts" Block mit 3 Zielen, Komma nach dem 1. und 2. Rechnerblock zur Trennung, aber nicht am Ende der Kette:

    "hosts" : 
        { 
            "Fritz!Box" : "fritz.box",
            "myMQTT"    : "192.168.224.123",
            "Internet"  : "myfritz.net"
        }

Den Hintergrunddienst *myPing* in einem Terminalfenster anlegen und starten:

    bash systemd/myPing.sh 
    
### Messwertprotokollierung in CSV-Dateien

Zum Protokollieren der von den Sensoren gemessenen Werte kann in den Sensorprogrammen das *log_topic* aktiviert werden. Daraufhin werden Nachrichten mit den Messwerten an den MQTT Broker geschickt.  
Das Python-Programm CsvFileLogMQTT.py dient zum Empfangen der Nachrichten und schreibt den Nachrichteninhalt in jeweils eine CSV-Datei pro Sensor(typ) in einem konfigurierbaren Verzeichnis.

Es muss sich um ein bereits bestehendes, vom Benutzer *pi* beschreibbares Verzeichnis handeln.

Für die Wertedateien ist eine tägliche Logrotation mit mehreren Generationen programmiert. Nach Mitternacht wird die aktuelle *.csv-Datei umbenannt zu *.1.csv, der Zähler der vorhandenen älteren Generationen wird um 1 erhöht. Die Gesamtzahl der zu haltenden Generationen ist konfigurierbar, zu alte Dateien werden gelöscht.

Die Konfiguration ist in der zentralen JSON Datei im Block *"CsvFileLog"* einzustellen, mögliche Optionen sind
- "CsvFileLog"
    - "debug" : "true" ( default ) für ausführliche Meldungen im Log, "false" unterdrückt die meisten Meldungen
    - "filePath" : absoluter Pfad zum Verzeichnis der CSV-Dateien, default = "/home/pi/sensordisplay/csv"
    - "generations" : ganzzahliger Wert der Anzahl der zu haltenden Tagesgenerationen, default = 7 

Den Hintergrunddienst *myCsvFileLog* in einem Terminalfenster anlegen und starten:

    bash systemd/myCsvFileLog.sh 
    
## Weiterleitung zu *Adafruit IO*

Die Weiterleitung der nach Aktivierung in den Sensorprogrammen erzeugten Nachrichten erfolgt im MQTT Broker. Dazu wird für Mosquitto eine Konfigurationsdatei mit einer *Bridge* Definition angelegt.  
Diese Konfiguration beinhaltet folgende benutzerspezifischen Einstellungen:

- den AdafruitIO Benutzernamen
- den AIO Key als Passwort
- die Zuordnungen von lokalen Topics zu Adafruit IO *feeds* für die gewünschten Weiterleitungen

Vorlage zur Erstellung der zusätzlichen *Mosquitto*-Konfigurationsdatei:

    ## Connection name
    connection adafruit
    address io.adafruit.com:8883
    bridge_capath /etc/ssl/certs/
    ## Credentials
    remote_username <Benutzername>
    remote_password <AIO Key des Benutzers>
    ## Config options for bridge
    start_type automatic
    bridge_protocol_version mqttv311
    notifications false
    try_private false
    ## Topics to bridge
    topic "" out 0 SensorDisplay/AdafruitIO/BH1750/brightness  <Benutzername>/feeds/<feed-key>
    topic "" out 0 SensorDisplay/AdafruitIO/BME280/temperature <Benutzername>/feeds/<feed-key>
    topic "" out 0 SensorDisplay/AdafruitIO/BME280/pressure    <Benutzername>/feeds/<feed-key>
    topic "" out 0 SensorDisplay/AdafruitIO/BME280/humidity    <Benutzername>/feeds/<feed-key>
    topic "" out 0 SensorDisplay/AdafruitIO/DS18B20/<28-nnnnnnnnnnnn> <Benutzername>/feeds/<feed-key>
    topic "" out 0 SensorDisplay/AdafruitIO/DS18B20/<28-nnnnnnnnnnnn> <Benutzername>/feeds/<feed-key>
    topic "" out 0 SensorDisplay/AdafruitIO/ping/<name>        <Benutzername>/feeds/<feed-key>
    topic "" out 0 SensorDisplay/AdafruitIO/ping/<name>        <Benutzername>/feeds/<feed-key>

Vorgehensweise zur Aktivierung der *Adafruit IO* Weiterleitung:
1. in der Vorlage die Felder <...> durch die entspechenden Daten ersetzen
2. für jeden gewünschten DS18B20-Sensor oder jeden 'ping'-Zielrechner eine eigene Zeile anlegen
3. nicht gewünschte Topics auskommentieren
4. geänderte Vorlage mit *sudo*-Rechten speichern als Datei `/etc/mosquitto/conf.d/AdafruitIO.conf`
5. *Mosquitto* zur Aktualisierung durchstarten mit `sudo systemctl restart mosquitto`

Achtung: mindestens eine Weiterleitung = topic muss konfiguriert sein, wenn die Konfiguration abgespeichert wird - ansonsten bricht der Start von *mosquitto* ohne Fehlermeldung(!) ab.
Zum Vermeiden jeglicher Weiterleitung die Datei `/etc/mosquitto/conf.d/AdafruitIO.conf` löschen oder die Dateiendung `.conf` umbenennen.















