"""SensorDisplay: module for storing data in CSV files.

Receiving sensor measurement data through local
MQTT broker instance and write it to CSV files.
Manages a configurable number of daily file generations.

source: https://gitlab.com/degraushe/sensordisplay
file: <installdir>/sensordisplay/CsvFileLogMQTT.py
"""

import paho.mqtt.client as mqtt
import time
import datetime
import sys
import pathlib
import os
import signal
from configuration import read_CsvFileLog_configuration

## initializing
log_topic = "SensorDisplay/CsvFileLog"
currentDay = datetime.datetime.now().day
max_generations = 9

## reading configuration
cf=read_CsvFileLog_configuration()
if cf.DEBUG:
    print( "CSV file path: ", cf.filePath )
    print( "generations  : ", cf.generations )
# reload of configuration on SIGHUP
def reload_configuration(sig, frame):
    global cf
    cf=read_CsvFileLog_configuration()
    if cf.DEBUG: print("reloaded configuration")
signal.signal(signal.SIGHUP, reload_configuration)

# clean exit on process termination
def signal_handler(sig, frame):
    raise KeyboardInterrupt
signal.signal(signal.SIGTERM, signal_handler)

# clean exit on process termination
import signal
def signal_handler(sig, frame):
    raise KeyboardInterrupt
signal.signal(signal.SIGTERM, signal_handler)

# misc
delay = 15.0

# MQTT client
client = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2,"CsvFileLog")

# The callback for when the client receives a CONNACK response from the server.
@client.connect_callback()
def on_connect(client, userdata, flags, reason_code, properties):
    if cf.DEBUG: print(f"Connected with result code {reason_code}")
    #
    client.subscribe(log_topic + "/#")

# The callback for when a PUBLISH message is received from the server.
@client.message_callback()
def on_message(client, userdata, msg):
    if cf.DEBUG: print(msg.topic+" "+str(msg.payload))
    topic = msg.topic.split('/')[-1].lower()
    message = msg.payload.decode()
    try:
        with open( cf.filePath + "/" + topic + ".csv", 'a' ) as f:
            f.write( message + "\n")
    except:
        if cf.DEBUG: print("write error !")

# The callback for clean up on disconnect called.
@client.disconnect_callback()
def on_disconnect(client, userdata, flags, reason_code, properties):
    if reason_code == 0:
        if cf.DEBUG: print("Bye")
    else:
        if cf.DEBUG: print("Unexpected disconnection.")
    
# connect to MQTT
client.connect("localhost", 1883, 60)

# start MQTT threaded message loop
client.loop_start()

# loop
while True:
    try:
        newDay = datetime.datetime.now().day
        if  newDay > currentDay:
            # csv log rotation - keep configured generations of history files, max. 9
            generations = min(cf.generations, max_generations)
            #
            if generations < max_generations:
                obsoleteList=[ str(name) for name in
                              list(pathlib.Path(cf.filePath).glob('*.['+str(generations+1)+'-9].csv')) ]
                if cf.DEBUG: print("List of obsolete files to remove: ", obsoleteList)
                for name in obsoleteList:
                    os.remove(name)
            #
            if generations > 1:
                historyList=[ str(name) for name in
                              list(pathlib.Path(cf.filePath).glob('*.[1-'+str(generations-1)+'].csv')) ]
                historyList.sort(reverse=True)
                if cf.DEBUG: print("List of history files to rename: ", historyList)
                for name in historyList:
                    os.replace(name,name[0:-5]+str(int(name[-5:-4])+1)+'.csv')
            #
            if generations != 0:
                currentList=[ str(name) for name in
                              list(pathlib.Path(cf.filePath).glob('*[!.]?.csv')) ]
                if cf.DEBUG: print("List of current files to rename: ", currentList)
                for name in currentList:
                    os.replace(name,name[0:-4]+'.1.csv')
            #
            currentDay = newDay
            #
        time.sleep(delay)
    except KeyboardInterrupt:
        break
    
# finalyse
client.loop_stop()
time.sleep(1)
client.disconnect()
sys.exit(0)
